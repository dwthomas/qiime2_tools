#! /usr/bin/env python
from qiime2 import Artifact
from pandas import DataFrame
table = Artifact.load("table.qza").view(DataFrame)
taxonomy = Artifact.load("taxonomy.qza").view(DataFrame)
table = table.rename(index=str, columns=taxonomy.to_dict()['Taxon'])
table = table.groupby(axis=1, level=0).sum()
table = Artifact.import_data("FeatureTable[Frequency]", table)
table.save("taxa_table.qza")
