#!/usr/bin/env python3
from qiime2 import Artifact
from pandas import DataFrame
from Bio import Phylo
from skbio.tree import TreeNode as TreeNode
import sys

node = Artifact.load(sys.argv[1]).view(TreeNode)
taxa = Artifact.load(sys.argv[2]).view(DataFrame)
for t in node.tips():
    t.name = taxa.loc[t.name]["Taxon"]
newtree = Artifact.import_data("Phylogeny[Rooted]",node)
newtree.save(sys.argv[3])
