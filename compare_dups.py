#! /usr/bin/env python
import matplotlib
matplotlib.use('MacOSX')
from scipy import optimize
import re
from qiime2 import Artifact
import numpy as np
from pandas import DataFrame
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

with open("augmented_metadata.tsv") as map:
    map = map.readlines()

fpd = {}
with open("core-metrics-results/5a157d78-3226-429f-b78a-c30944fbfe12/data/alpha-diversity.tsv") as fd:
    fd = fd.readlines()
al = []
for line in fd[1:]:
    words = line.strip().split()
    fpd[words[0]] = float(words[1])
    al.append(float(words[1]))
duplicates = {}
for line in map[1:]:
    sample = line.split("\t")[0]
    loc = " ".join(line.split("\t")[1:3])
    try:
        if loc in duplicates:
            duplicates[loc].append(fpd[sample])
        else:
            duplicates[loc] = [fpd[sample]]
    except:
        pass
print(duplicates)
al = np.asarray(al)
plt.plot(0*al, al, ".k", alpha = .6)
labels = ["all samples"]
i = 1
for key in duplicates:
    tp = np.asarray(duplicates[key])
    plt.plot(0*tp + i, tp, ".k", alpha = .6)
    i += 1
    labels.append(key + ": " + str(len(tp
    )))
plt.xticks(np.arange(len(labels)), labels, rotation = 90)
plt.xlabel("Sample")
plt.ylabel("Faith PD")
plt.tight_layout()
plt.show()
