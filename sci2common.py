#!/usr/bin/env python
import pickle
import requests
import os
import argparse
import sqlite3
import tqdm

try:
    with open('/tmp/sci2common/wikisci2common_cache.pickle', 'rb') as handle:
        tdict = pickle.load(handle)
except:
    tdict = {"Apeltes quadracus": "Fourspine stickleback",
             "Botryllus schlosseri": "golden star tunicate",
             "Chondrus crispus": "Irish Moss",
             "Fucus vesiculosus": "bladderwrack",
             "Halichondria panicea": "breadcrumb sponge",
             "Unassigned": "Unassigned"
    }

    
def get_wikipedia(query):
    """
    Where query is the string to query (I'd suggest lowercase and stripped of extra 
    spaces but you do you.
    Returns string article title if sucessful.
    """
    uquery = query.replace(" ", "%20")
    url = "".join(["https://en.wikipedia.org/w/api.php?format=json&",
                   "action=query&", "prop=extracts&",
                   "exintro=&", "explaintext=&",
                   "redirects=1&", "titles=", uquery])
    r = requests.get(url)
    if float(list(r.json()["query"]["pages"].keys())[0]) < 0:
        return ""
    else:
        return list(r.json()["query"]["pages"].values())[0]["title"]

def get_itis(query, c):
    query = query[0].upper() + query[1:]
    c.execute("SELECT vernacular_name FROM sci2common WHERE completename = ?;", (query,))
    ret = c.fetchone()
    if ret == None:
        return ""
    else:
        return ret[0]
    
def get(query):
    with  sqlite3.connect("file:/usr/local/share/databases/ITIS.sqlite?mode=ro", uri=True) as conn:
        c = conn.cursor()
        c.execute("CREATE TEMPORARY TABLE sci2common AS SELECT completename, vernacular_name  FROM longnames JOIN vernaculars ON longnames.tsn = vernaculars.tsn WHERE  language = 'English';")
        c.execute("CREATE INDEX mytable_ind ON sci2common (completename, vernacular_name);")
        if query not in tdict:
            tdict[query] = get_itis(query, c)
    return # TODO: dict[query]


def get_all(queries, pos = 0):
    with  sqlite3.connect("file:/usr/local/share/databases/ITIS.sqlite?mode=ro", uri=True) as conn:
        c = conn.cursor()
        c.execute("CREATE TEMPORARY TABLE sci2common AS SELECT completename, vernacular_name  FROM longnames JOIN vernaculars ON longnames.tsn = vernaculars.tsn WHERE  language = 'English';")
        c.execute("CREATE INDEX mytable_ind ON sci2common (completename, vernacular_name);")
        for i in tqdm.trange(len(queries), position = pos):
            q = queries[i]
            if q not in tdict:
                tdict[q] = get_itis(q, c)
            if tdict[q] != "":
                queries[i] = tdict[q]
    return queries

if __name__ == "__main__":
    """
    Parse arguments and get queries.
    """

    argParser = argparse.ArgumentParser(
        description='Use wikipedia titles to convert scientific names to common names',
        prog='sci2common'
    )
    argParser.add_argument("sci_names", nargs="+")
    arguments = argParser.parse_args()
    queries = []
    for query in arguments.sci_names:
        # get query sequence
        queries.append(query.lower().strip())
    res = get_all(queries)
    for i in range(len(queries)):    
        print(queries[i] + " : " + res[i])
    # Try to load the cache file
if not os.path.exists('/tmp/sci2common/'):
    try:
        os.makedirs('/tmp/sci2common/')
    except:
        pass
try:
    with open('/tmp/sci2common/wikisci2common_cache.pickle', 'wb') as handle:
        pickle.dump(tdict, handle, protocol=pickle.HIGHEST_PROTOCOL)
except:
    pass
