#!/usr/bin/env python
import glob
import os
import shutil
os.mkdir("reads")
for f in glob.glob("*/*.gz"):
    shutil.copy2(f, "reads/" + f.split("/")[1])
for f in glob.glob("reads/*R3*"):
    os.rename(f, f.replace("R3", "R2"))
