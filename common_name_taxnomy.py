#!/usr/bin/env python
from qiime2 import Artifact
from pandas import DataFrame
import sci2common
import glob
import sys
from multiprocessing import Pool

def mkart(filepath):
    tax = filepath[0]
    pos = filepath[1]
    taxA = Artifact.load(tax)
    taxa = taxA.view(DataFrame)
    """
    for i in range(len(taxa["Taxon"])):
        t = taxa["Taxon"][i]
        s2c = sci2common.get(t)
        if s2c != "":
            taxa["Taxon"][i] = s2c
    """
    new_tax = sci2common.get_all(taxa["Taxon"], pos = pos)
    taxa["Taxon"] = new_tax
    art = Artifact.import_data("FeatureData[Taxonomy]", taxa)
    art.save("vernacular_" + tax)

if __name__ == "__main__":
    pool = Pool(len(sys.argv) - 1)
    pool.map(mkart, list(zip(sys.argv[1:], list(range(len(sys.argv[1:]))))))
