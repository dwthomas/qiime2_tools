#!/usr/bin/env python3
# coding: utf-8

# In[1]:


from qiime2 import Artifact
import pandas as pd


# In[2]:


ktaxa = pd.read_csv("kraken_out.txt", delimiter="\t", header = None, index_col = 1)


# In[3]:


ktaxa.index.name = "Feature ID"


# In[4]:


ktaxa = ktaxa.drop(0,axis = "columns").drop(3,axis = "columns").drop(4,axis = "columns")


# In[5]:


ktaxa.columns = ["Taxon"]


# In[6]:


ktaxa["Consensus"] = 1.0


# In[7]:


ktaxa["Confidence"] = 1.0


# In[8]:


taxa = Artifact.import_data("FeatureData[Taxonomy]", ktaxa)


# In[24]:


taxa.save("kraken_taxonomy.qza")


# In[12]:


ktaxa.Taxon.map(lambda x: " ".join(x.split()[0:2]).replace("(taxid", ""))


# In[9]:


